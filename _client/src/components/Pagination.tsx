import React from "react";
export interface PaginationProps {
  usersPerPage: any;
  users: any;
  paginate: any;
  currentPage: any;
}

const Pagination: React.FC<PaginationProps> = ({
  usersPerPage,
  users,
  paginate,
  currentPage,
}) => {
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(users.length / usersPerPage); i++) {
    pageNumbers.push(i);
  }
  console.log(usersPerPage, users, pageNumbers);

  return (
    <div className="pagination">
      <ul>
        {pageNumbers.map((number) => (
          <li key={number} className={currentPage === number ? "active" : ""}>
            <span onClick={() => paginate(number)} className="page-link">
              {number}
            </span>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Pagination;
