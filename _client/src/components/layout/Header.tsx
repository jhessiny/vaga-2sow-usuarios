import { useContext } from "react";
import { NavLink } from "react-router-dom";
import { UserContext } from "../../UserContext";

const Header = () => {
  const [loggedUser] = useContext(UserContext);
  return (
    <header>
      <a href="/">User Management</a>
      <nav>
        <ul>
          {loggedUser && (
            <>
              <li>
                <NavLink to="/">Lista de Usuários</NavLink>
              </li>
              <li>
                <NavLink to="/new-user">Novo Usuário</NavLink>
              </li>
            </>
          )}
        </ul>
      </nav>
    </header>
  );
};

export default Header;
