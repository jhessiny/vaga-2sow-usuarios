import { useEffect, useState } from "react";
import firebase from "../../firebase";

export interface EditUserPageProps {
  getSearch: any;
  showSearch: boolean;
}

const LoggedBar: React.FC<EditUserPageProps> = ({ getSearch, showSearch }) => {
  const [search, setSearch] = useState("");

  const submitSearchHandler = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    getSearch(search);
  };

  useEffect(() => {
    setSearch("");
  }, []);

  const logoutHandler = () => {
    firebase
      .auth()
      .signOut()
      .then(() => localStorage.removeItem("userToken"));
  };

  return (
    <div className="logged-bar">
      <div>
        {showSearch && (
          <form action="/" onSubmit={submitSearchHandler}>
            <input
              type="text"
              onChange={(e) => setSearch(e.target.value)}
              value={search}
            />
            <button>
              <i className="lni lni-search-alt"></i>
            </button>
          </form>
        )}
      </div>
      <div className="left">
        <p>Bem vindo!</p>
        <p className="logout" onClick={logoutHandler}>
          Sair
        </p>
      </div>
    </div>
  );
};

export default LoggedBar;
