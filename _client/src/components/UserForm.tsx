import { useEffect, useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { UserFormWrapper } from "../styles/UserForm.style";
import { Spinner } from "../styles/Spinner.style";
export interface UserFormProps {
  title: string;
  id: any;
  clickHandler: (
    e: React.MouseEvent<HTMLButtonElement>,
    userId: string,
    inputError: boolean
  ) => void;
  actionName: string;
  formData: any;
  changeHandler: (e: React.ChangeEvent<HTMLInputElement>) => void;
  formType: string;
}

const UserForm: React.FC<UserFormProps> = ({
  title,
  clickHandler,
  actionName,
  formData,
  id,
  changeHandler,
  formType,
}) => {
  const [nameError, setNameError] = useState<string>("");
  const [emailError, setEmailError] = useState<string>("");
  const [cpfError, setCpfError] = useState<string>("");
  const [cepError, setCepError] = useState<string>("");
  const [ruaError, setRuaError] = useState<string>("");
  const [numeroError, setNumeroError] = useState<string>("");
  const [bairroError, setBairroError] = useState<string>("");
  const [cidadeError, setCidadeError] = useState<string>("");
  const [isFetching, setIsFetching] = useState<boolean>(true);
  const [isSending, setIsSending] = useState<boolean>(false);

  const verifyInputs = () => {
    console.log(formData.endereco.cep);
    let cepOnlyNumbers = formData.endereco.cep
      .toString()
      .replace(/\D/g, "")
      .split("-")
      .join("");
    let inputError = false;
    if (!formData.nome || formData.nome.length < 1) {
      setNameError("Nome inválido.");
      inputError = true;
    }
    if (
      !formData.email ||
      formData.email.length < 1 ||
      !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
        formData.email
      )
    ) {
      setEmailError("Email inválido.");
      inputError = true;
    }
    if (!formData.cpf || formData.cpf.length < 11) {
      setCpfError("CPF inválido.");
      inputError = true;
    }
    if (
      !cepOnlyNumbers ||
      cepOnlyNumbers.length > 8 ||
      cepOnlyNumbers.length < 8
    ) {
      setCepError("Cep inválido.");
      inputError = true;
    }
    if (!formData.endereco.rua || formData.endereco.rua.length < 1) {
      setRuaError("Endereço inválido.");
      inputError = true;
    }
    if (!formData.endereco.numero || formData.endereco.numero.length < 1) {
      setNumeroError("Número inválido.");
      inputError = true;
    }
    if (!formData.endereco.bairro || formData.endereco.bairro.length < 1) {
      setBairroError("Bairro inválido.");
      inputError = true;
    }
    if (!formData.endereco.cidade || formData.endereco.cidade.length < 1) {
      setCidadeError("Cidade inválida.");
      inputError = true;
    }
    return inputError;
  };

  const submitHandler = (
    e: React.MouseEvent<HTMLButtonElement>,
    id: string
  ) => {
    e.preventDefault();
    setNameError("");
    setEmailError("");
    setCpfError("");
    setBairroError("");
    setNumeroError("");
    setCepError("");
    setCidadeError("");
    setRuaError("");
    let inputError = verifyInputs();
    clickHandler(e, id, inputError);
    console.log(inputError);
    if (!inputError) {
      setIsSending(true);
      toast.success("Usuário salvo.");
    } else {
      toast.error("Algo deu errado. Verifique os dados inseridos.");
    }
  };

  useEffect(() => {
    if (formType === "edit") {
      setTimeout(() => setIsFetching(false), 2100);
    } else {
      setIsFetching(false);
    }
  }, [formType]);

  return (
    <>
      <ToastContainer />
      <UserFormWrapper>
        <h1>{title}</h1>
        <form action="" className={isFetching ? "form-fetching" : ""}>
          <div className="input-wrapper">
            <label htmlFor="">Nome</label>
            <input
              onChange={(e) => changeHandler(e)}
              value={formData.nome}
              type="text"
              name="nome"
              required
              disabled={isFetching}
            />
            <p className="input-error">{nameError}</p>
          </div>
          <div className="input-wrapper">
            <label htmlFor="">Email</label>
            <input
              disabled={isFetching}
              required
              onChange={(e) => changeHandler(e)}
              value={formData.email}
              type="text"
              name="email"
            />
            <p className="input-error">{emailError}</p>
          </div>
          <div className="input-wrapper">
            <label htmlFor="">CPF</label>
            <input
              disabled={isFetching}
              required
              onChange={(e) => changeHandler(e)}
              value={formData.cpf}
              type="text"
              name="cpf"
            />
            <p className="input-error">{cpfError}</p>
          </div>
          <div className="row">
            <div className="input-wrapper">
              <label htmlFor="">CEP</label>
              <input
                disabled={isFetching}
                required
                onChange={(e) => changeHandler(e)}
                value={formData.endereco.cep}
                type="text"
                name="cep"
              />
              <p className="input-error">{cepError}</p>
            </div>
            <div className="input-wrapper">
              <label htmlFor="">Número</label>
              <input
                disabled={isFetching}
                required
                onChange={(e) => changeHandler(e)}
                value={formData.endereco.numero}
                type="text"
                name="numero"
              />
              <p className="input-error">{numeroError}</p>
            </div>
          </div>
          <div className="input-wrapper">
            <label htmlFor="">Rua</label>
            <input
              disabled={isFetching}
              required
              onChange={(e) => changeHandler(e)}
              value={formData.endereco.rua}
              type="text"
              name="rua"
            />
            <p className="input-error">{ruaError}</p>
          </div>
          <div className="row">
            <div className="input-wrapper">
              <label htmlFor="">Bairro</label>
              <input
                disabled={isFetching}
                required
                onChange={(e) => changeHandler(e)}
                value={formData.endereco.bairro}
                type="text"
                name="bairro"
              />
              <p className="input-error">{bairroError}</p>
            </div>
            <div className="input-wrapper">
              <label htmlFor="">Cidade</label>
              <input
                disabled={isFetching}
                required
                onChange={(e) => changeHandler(e)}
                value={formData.endereco.cidade}
                type="text"
                name="cidade"
              />
              <p className="input-error">{cidadeError}</p>
            </div>
          </div>
          <button
            className="spinning button"
            onClick={(e) => submitHandler(e, id)}
          >
            {isSending && (
              <Spinner>
                <div className=" lds-ring-sm">
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </Spinner>
            )}
            {actionName}
          </button>
        </form>
      </UserFormWrapper>
    </>
  );
};

export default UserForm;
