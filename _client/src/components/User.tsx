import { Link } from "react-router-dom";

export interface UserProps {
  nome: string;
  email: string;
  cpf: string;
  cidade: string;
  id: string;
  deleteHandler: (
    e: React.MouseEvent<HTMLButtonElement>,
    userId: string
  ) => void;
  isFetching: boolean;
}

const User: React.FC<UserProps> = ({
  nome,
  email,
  cpf,
  cidade,
  id,
  deleteHandler,
  isFetching,
}) => {
  return (
    <>
      <td className={`${isFetching ? "deleting-user" : ""}`}>{nome}</td>
      <td className={`${isFetching ? "deleting-user" : ""}`}>{cpf}</td>
      <td className={`${isFetching ? "deleting-user" : ""}`}>{email}</td>
      <td className={`${isFetching ? "deleting-user" : ""}`}>{cidade}</td>
      <td className={`${isFetching ? "deleting-user" : ""}`}>
        <button
          onClick={(e) => deleteHandler(e, id)}
          disabled={isFetching}
          className={`${isFetching ? "deleting-user" : ""}`}
        >
          <i className="lni lni-trash"></i>
        </button>
        <button disabled={isFetching}>
          <Link
            to={"/edit-user/" + id}
            style={{ pointerEvents: isFetching ? "none" : "all" }}
            className={`${isFetching ? "deleting-user" : ""}`}
          >
            <i className="lni lni-pencil"></i>
          </Link>
        </button>
      </td>
    </>
  );
};

export default User;
