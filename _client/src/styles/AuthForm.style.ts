import styled from "styled-components";

export const AuthFormWrapper = styled.div`
  color: #444;
  max-width: 30rem;
  font-size: 1.4rem;
  margin: 5rem auto;
  background-color: #f9f9f9;
  box-shadow: 3px 3px 3px #dddd;
  border-radius: 5px 5px 0 0;
  overflow: hidden;
  padding-bottom: 2rem;

  h1 {
    background-color: #009879;
    color: #f9f9f9;
    padding: 1rem;
    text-align: center;
  }

  form {
    padding: 3rem 2.5rem;
  }

  .input-wrapper {
    padding: 1rem 0;
    .input-error {
      font-size: 1.2rem;
      color: red;
    }
  }

  input {
    width: 100%;
    margin-top: 0.3rem;
    background-color: #ddd;
    border: none;
    padding: 0.7rem;
  }

  label {
    display: block;
  }

  button {
    background-color: #009879;
    color: #f9f9f9;
    padding: 0.8rem 1rem;
    border: none;
    float: right;
    margin-top: 2rem;
    &:hover {
      cursor: pointer;
      background-color: #009879;
    }
  }
`;
