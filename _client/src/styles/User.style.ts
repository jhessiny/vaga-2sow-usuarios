import styled from "styled-components";

export const UsersTable = styled.table`
  border-collapse: collapse;
  min-width: 50rem;
  margin: 5rem auto 1rem auto;
  border-radius: 10px;
  overflow: hidden;
  text-align: left;
  font-size: 1.2rem;
  th,
  td {
    padding: 1.5rem 1rem;
  }

  .deleting-user {
    color: #ccc;
  }

  thead {
    width: 100%;
    tr {
      background-color: #009879;
      color: #fff;
      font-weight: bold;
    }
  }
  tbody {
    position: relative;
    tr {
      border-bottom: 1px solid #dddd;
      &:hover {
        background-color: #eee;
      }
    }
    tr:nth-child(even) {
      background-color: #eee;
    }
    tr:last-child {
      border-bottom: 2px solid #009879;
    }
  }
`;
