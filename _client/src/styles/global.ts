import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    *{
         margin: 0;
         padding: 0;
         box-sizing: border-box;
    }
    html{
        font-size: 62.5%;
        font-family: Arial, Helvetica, sans-serif;
        min-height: 100vh;
        position: relative;
    }
    ul{
        list-style: none;
    }
    header{
        font-size: 1.6rem;
        background-color: #eeeeee;
        display: flex;
        justify-content: space-between;
        align-items: center;
        box-shadow: 3px 3px 5px #ccc;
        a{
            color: black;
            text-decoration: none;
            padding: 2rem 1rem;
        }
        ul{
            display: flex;
            a{
            &:hover, 
            a.active{
                background-color: #ccc;
                }
            }
        }
    }
    main{
    padding-bottom: 7rem;
    table{
        position: relative;
    }

    
    .pagination ul{
        display: flex;
        justify-content: center;
        li{
            border: 1px solid #bbb;
            padding: 2px 5px;
            color: #444;
            font-size: 1.4rem;
            color: #333;
            cursor: pointer;
        }
        li.active{
            background-color: #bbb;
        }
        
    }
    }
    .logged-bar{
        display: flex;
        justify-content: space-between;
        padding: 0 2rem;
        font-size: 1.4rem;
        box-shadow: 3px 3px 5px #bbb;
      
        form{
            padding: 1.2rem 1rem 1rem 1rem;
           
            
            input{
                border: 1px solid #bbb;
                border-right: none;
                padding: .5rem;
                outline: none;
                border-radius: .3rem 0 0 .3rem;
            }
            button{
                margin-left: -2px;
                border: 1px solid #bbb;
                border-left: none;
                background-color: transparent;
                border-radius: 0 .3rem .3rem 0;
                padding: .5rem;
                outline: none;
            }
        }
        .left{
            display: flex;
            padding: .5rem;
        }
        p{
            padding: 1.2rem 1rem 1rem 1rem;
        }
        .logout{
            border-bottom: 2px solid #fff;
            &:hover{
                border-bottom: 2px solid #444;
                cursor: pointer;
            }
        }
    }
    footer{
        font-size: 1.6rem;
        background-color: #eeeeee;
        width: 100%;
        padding: 2rem;
        text-align: center;
        position: absolute;
        bottom: 0;
       
        }
    `;
