export const fetchUsers: any = async (url: string) => {
  let fetchUrl = "http://localhost:5000/usuarios";
  if (url) {
    fetchUrl = url;
  }
  return fetch(fetchUrl)
    .then((res) => res.json())
    .then((data: any) => {
      return data;
    });
};
