import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyB1BY0SJmscfVVqErHnfHUs9yTwXv5mE7s",
  authDomain: "cadastro-usuario-react.firebaseapp.com",
  projectId: "cadastro-usuario-react",
  storageBucket: "cadastro-usuario-react.appspot.com",
  messagingSenderId: "57810578534",
  appId: "1:57810578534:web:94086a1d015d6cd48a51a7",
};

export default firebase.initializeApp(firebaseConfig);
