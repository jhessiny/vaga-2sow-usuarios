import { useContext, useEffect, useState } from "react";
import { AuthFormWrapper } from "../styles/AuthForm.style";
import { UserContext } from "../UserContext";
import firebase from "../firebase";
import { Redirect } from "react-router";

export interface LoginPageProps {}

const LoginPage: React.FC<LoginPageProps> = () => {
  const [loggedUser, setLoggedUser] = useContext(UserContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwordError, setPasswordError] = useState("");

  const authListener = () => {
    firebase.auth().onAuthStateChanged((user: any) => {
      if (user) {
        setLoggedUser(user);
      } else {
        setLoggedUser(null);
      }
    });
  };

  useEffect(() => {
    authListener();
  }, [loggedUser]);

  const loginHandler = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();
    // clearErrors();
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((user) => {
        if (user.user?.refreshToken)
          localStorage.setItem("userToken", user.user?.refreshToken);
      })
      .catch((err) => {
        switch (err.code) {
          case "auth/invalid-email":
          case "auth/user-disabled":
          case "auth/user-not-found":
            setEmailError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
          default:
            break;
        }
      });
  };

  let loginPage;
  if (loggedUser) {
    loginPage = <Redirect to="/" />;
  }

  return (
    <AuthFormWrapper>
      {loginPage}
      <h1>Login</h1>
      <form className="form-login" action="">
        <div className="input-wrapper">
          <label htmlFor="">Email</label>
          <input
            type="email"
            required
            value={email}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setEmail(e.target.value)
            }
          />
          <p className="input-error">{emailError}</p>
        </div>
        <div className="input-wrapper">
          <label htmlFor="">Senha</label>
          <input
            type="password"
            required
            value={password}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setPassword(e.target.value)
            }
          />
          <p className="input-error">{passwordError}</p>
        </div>
        <button onClick={loginHandler}>Login</button>
      </form>
    </AuthFormWrapper>
  );
};

export default LoginPage;
