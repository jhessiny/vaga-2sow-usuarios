import { useContext, useState } from "react";
import UserForm from "../components/UserForm";
import { v4 as uuidv4 } from "uuid";
import { Redirect } from "react-router";
import { UserContext } from "../UserContext";
import LoggedBar from "../components/layout/loggedBar";
const uuid = uuidv4();

export interface NewUserPageProps {
  history: any;
}

const NewUserPage: React.FC<NewUserPageProps> = (props) => {
  const [loggedUser] = useContext(UserContext);
  const [addUserForm, setAddUserForm] = useState({
    nome: "",
    id: uuid,
    cpf: "",
    email: "",
    endereco: {
      cep: "",
      rua: "",
      numero: "",
      bairro: "",
      cidade: "",
    },
  });
  const changeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = e.target.value;
    let inputName = e.target.name;
    if (inputName === "nome" || inputName === "email" || inputName === "cpf") {
      setAddUserForm((prevState: any) => {
        return { ...prevState, [inputName]: inputValue };
      });
    } else {
      setAddUserForm((prevState: any) => {
        return {
          ...prevState,
          endereco: { ...prevState.endereco, [inputName]: inputValue },
        };
      });
    }
  };

  const addUser = (
    e: React.MouseEvent<HTMLButtonElement>,
    id: any,
    inputError: boolean
  ) => {
    e.preventDefault();
    if (inputError === false) {
      console.log(inputError);
      fetch("http://localhost:5000/usuarios/", {
        method: "POST",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify(addUserForm),
      }).then(() => props.history.push("/"));
    }
  };

  let redirect;
  if (!loggedUser) {
    redirect = <Redirect to="/login" />;
  }

  return (
    <div>
      <LoggedBar getSearch="" showSearch={false} />
      {redirect}
      <UserForm
        title="Cadastrar novo usuário"
        clickHandler={addUser}
        actionName="Cadastrar"
        formData={addUserForm}
        id=""
        changeHandler={(e) => changeInput(e)}
        formType={"new"}
      />
    </div>
  );
};

export default NewUserPage;
