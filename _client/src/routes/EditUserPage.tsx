import { useContext, useEffect, useState } from "react";
import UserForm from "../components/UserForm";
import { fetchUsers } from "../api/index";
import { toast } from "react-toastify";
import LoggedBar from "../components/layout/loggedBar";
import { UserContext } from "../UserContext";
import { Redirect } from "react-router";

export interface EditUserPageProps {}

const EditUserPage: React.FC<EditUserPageProps> = (props: any) => {
  const [loggedUser] = useContext(UserContext);
  const { match } = props;
  const [userId, setUserId] = useState<any>(null);
  const [timer, setTimer] = useState<any>(null);
  const [editFormData, setEditForm] = useState({
    nome: "",
    cpf: "",
    email: "",
    endereco: {
      cep: "",
      rua: "",
      numero: "",
      bairro: "",
      cidade: "",
    },
  });

  useEffect(() => {
    let id = match.params.id;
    setUserId(id);
    let usersData: any;
    const getFetchedData = async () => {
      usersData = await fetchUsers();
      let curUser = await usersData.find((user: any) => user.id === id);
      setEditForm(curUser);
    };
    getFetchedData();
  }, [match]);

  const saveChanges = (
    e: React.MouseEvent<HTMLButtonElement>,
    userId: string,
    inputError: boolean
  ) => {
    e.preventDefault();
    if (!inputError) {
      fetch("http://localhost:5000/usuarios/" + userId, {
        method: "PUT",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
        body: JSON.stringify(editFormData),
      }).then(() => props.history.push("/"));
    }
  };

  const changeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    console.log("oi");
    const inputValue = e.target.value;
    let inputName = e.target.name;
    if (inputName === "nome" || inputName === "email" || inputName === "cpf") {
      setEditForm((prevState: any) => {
        return { ...prevState, [inputName]: inputValue };
      });
    } else {
      setEditForm((prevState: any) => {
        return {
          ...prevState,
          endereco: { ...prevState.endereco, [inputName]: inputValue },
        };
      });
      if (inputName === "cep") {
        clearTimeout(timer);

        setTimer(
          setTimeout(() => {
            let cepOnlyNumbers = inputValue
              .replace(/\D/g, "")
              .split("-")
              .join("");
            if (cepOnlyNumbers.length > 8 || cepOnlyNumbers.length < 8) {
              return toast.error("Cep inválido.");
            }
            const cepUrl = `https://viacep.com.br/ws/${cepOnlyNumbers}/json/`;
            fetch(cepUrl)
              .then((res) => res.json())
              .then((data: any) => {
                setEditForm({
                  ...editFormData,
                  endereco: {
                    ...editFormData.endereco,
                    cep: data.cep,
                    rua: data.logradouro,
                    bairro: data.bairro,
                    cidade: data.localidade,
                  },
                });
                return toast.error("Cep válido.");
              });
          }, 1000)
        );
      }
    }
  };

  let redirect;
  if (!loggedUser) {
    redirect = <Redirect to="/login" />;
  }

  return (
    <div>
      {redirect}
      <LoggedBar getSearch="" showSearch={false} />
      <UserForm
        title="Editar usuário"
        clickHandler={saveChanges}
        actionName="Salvar"
        formData={editFormData}
        id={userId}
        changeHandler={changeInput}
        formType={"edit"}
      />
    </div>
  );
};

export default EditUserPage;
