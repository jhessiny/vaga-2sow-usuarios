import { useEffect, useState, useContext } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import User from "../components/User";
import { UsersTable } from "../styles/User.style";
import { fetchUsers } from "../api/index";
import { Spinner } from "../styles/Spinner.style";
import LoggedBar from "../components/layout/loggedBar";
import { UserContext } from "../UserContext";
import { Redirect } from "react-router";
import Pagination from "../components/Pagination";

export interface UsersListPageProps {}

const UsersListPage: React.FC<UsersListPageProps> = () => {
  const [loggedUser] = useContext(UserContext);
  const [isFetching, setIsFetching] = useState(false);
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [usersPerPage] = useState(10);

  const getFetchedData = (url: any) => {
    fetchUsers(url)
      .then((data: any) => setUsers(data))
      .finally(() => setIsFetching(false));
  };

  useEffect(() => {
    setIsFetching(true);
    getFetchedData(null);
  }, []);

  const indexOfLastUser = currentPage * usersPerPage;
  const indexOfFirstUser = indexOfLastUser - usersPerPage;
  const currentUsers = users.slice(indexOfFirstUser, indexOfLastUser);

  const paginate = (pageNumber: any) => {
    setCurrentPage(pageNumber);
  };

  const getSearch = (search: string) => {
    setIsFetching(true);
    let url = `http://localhost:5000/usuarios?q=${search}`;
    fetchUsers(url)
      .then((data: any) => setUsers(data))
      .finally(() => setIsFetching(false));
  };

  const deleteHandler = async (
    e: React.MouseEvent<HTMLButtonElement>,
    userId: string
  ) => {
    e.preventDefault();
    setIsFetching(true);
    fetch("http://localhost:5000/usuarios/" + userId, {
      method: "DELETE",
    })
      .then((res) => {
        getFetchedData(null);
      })
      .then(() => setIsFetching(false))
      .then(() => toast.success("Usuário deletado."))
      .catch(() => toast.error("Algo deu errado."));
  };

  let redirect = <div></div>;
  if (!loggedUser) {
    redirect = <Redirect to="/login" />;
  }

  return (
    <div>
      {redirect}

      <ToastContainer />
      <LoggedBar getSearch={getSearch} showSearch />
      {!users.length && isFetching ? (
        <Spinner>
          <div className="lds-ring">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </Spinner>
      ) : (
        <>
          <UsersTable>
            <thead>
              <tr>
                <th>Nome</th>
                <th>CPF</th>
                <th>Email</th>
                <th>Cidade</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              {users &&
                currentUsers.map((user: any, index) => (
                  <tr key={"user-" + index}>
                    <User
                      nome={user.nome}
                      email={user.email}
                      cpf={user.cpf}
                      cidade={user.endereco.cidade}
                      id={user.id}
                      deleteHandler={deleteHandler}
                      isFetching={isFetching}
                    />
                  </tr>
                ))}
            </tbody>
          </UsersTable>
          <Pagination
            users={users}
            usersPerPage={usersPerPage}
            paginate={paginate}
            currentPage={currentPage}
          />
        </>
      )}
      {!users ||
        (!users.length && !isFetching && (
          <p
            style={{
              padding: "0 10px",
              textAlign: "center",
              display: "block",
              width: "100%",
            }}
          >
            Nenhum usuário encontrado.
          </p>
        ))}
      {!users && isFetching}
    </div>
  );
};

export default UsersListPage;
