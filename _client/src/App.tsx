import Footer from "./components/layout/Footer";
import Header from "./components/layout/Header";
import { GlobalStyle } from "./styles/global";
import { Route, Switch } from "react-router-dom";

import UsersListPage from "./routes/UsersListPage";
import NewUserPage from "./routes/NewUserPage";
import EditUserPage from "./routes/EditUserPage";
import LoginPage from "./routes/LoginPage";
import { UserProvider } from "./UserContext";

export interface AppProps {}

const App: React.FC<AppProps> = () => {
  return (
    <UserProvider>
      <GlobalStyle />
      <div className="App">
        <Header />

        <main>
          <Switch>
            <Route exact path="/" component={UsersListPage} />
            <Route path="/new-user" component={NewUserPage} />
            <Route path="/edit-user/:id" component={EditUserPage} />
            <Route path="/login" component={LoginPage} />
          </Switch>
        </main>
        <Footer />
      </div>
    </UserProvider>
  );
};

export default App;
