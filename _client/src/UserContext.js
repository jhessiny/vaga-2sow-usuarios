import { useState, createContext } from "react";

export const UserContext = createContext();

export const UserProvider = (props) => {
  const [loggedUser, setLoggedUser] = useState("");

  return (
    <div>
      <UserContext.Provider value={[loggedUser, setLoggedUser]}>
        {props.children}
      </UserContext.Provider>
      ;
    </div>
  );
};
